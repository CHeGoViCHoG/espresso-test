package calculator.tryparse.universalcalculator_v2;


import android.os.SystemClock;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class CheckSum extends BaseTest {
    @Test
    public void resultSum() {
        onView(withText("Калькулятор")).check(matches(isDisplayed()));
        onView(withId(R.id.btn7)).perform(click());
        onView(withId(R.id.btn8)).perform(click());
        onView(withId(R.id.btn0)).perform(click());
        onView(withId(R.id.btn0)).perform(click());
        SystemClock.sleep(1500);
        onView(withId(R.id.btnSum)).perform(click());
        onView(withId(R.id.btn2)).perform(click());
        onView(withId(R.id.btn0)).perform(click());
        onView(withId(R.id.btn0)).perform(click());
        onView(withId(R.id.btnEquals)).perform(click());
        //  onView(withText("8000")).check(matches(isDisplayed()));
        onView(withId(R.id.editText2)).check(matches(withText("8000.0")));
        SystemClock.sleep(1500);
        takeScreenshot("test_1");
    }
}



