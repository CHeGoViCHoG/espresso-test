package calculator.tryparse.universalcalculator_v2

import android.app.Activity
import android.view.View
import androidx.test.espresso.Espresso
import androidx.test.espresso.FailureHandler
import androidx.test.espresso.Root
import androidx.test.espresso.base.DefaultFailureHandler
import androidx.test.espresso.core.internal.deps.guava.collect.Iterables
import androidx.test.espresso.matcher.RootMatchers.isPlatformPopup
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import com.jraska.falcon.FalconSpoon
import org.hamcrest.Matcher
import org.junit.Rule


abstract class BaseTest {
    protected val context = getInstrumentation().targetContext

//    protected val uiDevice = UiDevice.getInstance(getInstrumentation())
//    protected val uiSelector = UiSelector()

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @get:Rule
    var readRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)

    @get:Rule
    var writeRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

    init {
        Espresso.setFailureHandler(object : FailureHandler {
            private var errorHandled = false

            override fun handle(error: Throwable?, viewMatcher: Matcher<View>?) {
                if (errorHandled == false) {
                    errorHandled = true
                    takeScreenshot("error")
                }

                DefaultFailureHandler(context).handle(error, viewMatcher)
            }
        })
    }

    open fun isPopupWindow(): Matcher<Root?>? {
        return isPlatformPopup()
    }

    protected fun takeScreenshot(tag: String) {
        try {
            FalconSpoon.screenshot(getCurrentActivity(), tag)
        } catch (e: Exception) {
        }
    }

    @Throws(Throwable::class)
    open fun getCurrentActivity(): Activity? {
        getInstrumentation().waitForIdleSync()
        val activity: Array<Activity?> = arrayOfNulls(1)

        getInstrumentation().runOnMainSync {
            val activities: Collection<Activity> = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
            activity[0] = Iterables.getOnlyElement(activities)
        }

        return activity[0]
    }
}