package calculator.tryparse.universalcalculator_v2

import android.os.SystemClock
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.junit.Test
import org.junit.runner.RunWith



@LargeTest
@RunWith(AndroidJUnit4::class)

class ScreenViewTest : BaseTest() {
    @Test
    fun viewFirstScreen() {
        //константная переменная
        val defaultSleep = SystemClock.sleep(2000)
        //находим текст в контенте (смотрится в tools\ layout inspector\ кликаем на объект\ смотрим в Properties
        // !!!!!!!!!!!!!!! (почему-то переменная юзалась только один раз , в остальное время переменные
        // не использовались и система не могла найти матчер , когда я юзал переменную метода несколько раз)
        // val clickPopupMenu = onView(withContentDescription("Ещё")).perform(click())

        // проверка наличия текста "калькулятор" на экране

        onView(withText("Калькулятор")).check(matches(isDisplayed()))
        onView(withContentDescription("Ещё")).perform(click())
        // вытаскиваем текст из PopupMenu -> .inRoot(isPopupWindow())
        onView(withText("Калькулятор")).inRoot(isPopupWindow()).perform(click())
        defaultSleep

        onView(withContentDescription("Ещё")).perform(click())
        onView(withText("Производные")).inRoot(isPopupWindow()).perform(click())
        defaultSleep

        onView(withText("Производные")).check(matches(isDisplayed()))
        onView(withContentDescription("Ещё")).perform(click())
        onView(withText("Логарифмы")).inRoot(isPopupWindow()).perform(click())
        defaultSleep

        onView(withText("Логарифмы")).check(matches(isDisplayed()))
        onView(withContentDescription("Ещё")).perform(click())
        onView(withText("Интегралы")).inRoot(isPopupWindow()).perform(click())
        defaultSleep

        onView(withText("Интегралы")).check(matches(isDisplayed()))
        onView(withContentDescription("Ещё")).perform(click())
        onView(withText("Справка")).inRoot(isPopupWindow()).perform(click())
        defaultSleep
    }
}
